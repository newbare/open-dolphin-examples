= Errorhandling

== Development

IntelliJ IDEA: open webapp/build.gradle

== Usage

* open a terminal and cd to the _webapp_ folder.
* invoke `./gradlew :appRun`
* point your browser to _http://localhost:8080/appContext/hellodolphin.jsp_
* Click on the _Greet_ button and the webpage should work as expected
* Now enter _tryboom_ into the textfield, click the _Greet_ button which should result into the message: _Exception caught: boom_

Note that an exception has been thrown during the Action's execution but also caught by the Action.
Therefore the application continues to work as expected.
Now we will cause the Action to throw an exception without catching it.

* Enter _boom_ into the textfield, click the _Greet_ button. A dialog will appear which says _Could not fetch /appContext/dolphin/, kind: application, httpStatus: 500_

Note that at this point in time the application is broken.


== How it works

On the client side we register an errorhandler when OpenDolphin is initialized:

[source,javascript]
.hellodolphin.jsp
----
var errorHandler = function (evt) {
  alert("Could not fetch " + evt.url
    + ", kind: " + evt.kind
    + ", httpStatus: " + evt.httpStatus
  );
};

var dolphin = opendolphin.makeDolphin()
  .url(odConfig.DOLPHIN_URL)
  .reset(true)
  .errorHandler(errorHandler)
  .build();
----

In the action we check the value of the _name_ attribute.
When it is _tryboom_ an exception is thrown in _checkName()_ (in a real application this routine is in a deep nested place) but caught by the action.
When it is just _boom_ an exception is thrown but not caught by the action.
In the first case the application continues to work in the second it is broken.

[source,java]
.ApplicationAction
----
actionRegistry.register(ApplicationConstants.COMMAND_GREET, (command, response) -> {
    String name = (String) getServerDolphin().getAt(PM_APP).getAt(ATT_NAME).getValue();
    ServerAttribute greeting = getServerDolphin().getAt(PM_APP).getAt(ATT_GREETING);

    // Use 'tryboom' as name to throw exception outside of try/catch:
    if (! name.startsWith("try")) {
      checkName(name);
    }

    // Use 'boom' as name to throw exception inside of try/catch:
    try {
      checkName(name);
      greeting.setValue("Hey " + name + " !");
    } catch (Exception e) {
      greeting.setValue("Exception caught: " + e.getMessage());
    }
  }
});

...

  private void checkName(String name) {
    if (name.contains("boom")) {
      throw new RuntimeException("boom");
    }
  }

----

An exception occuring during the execution of an action should not break the whole application.
Therefore I think at the moment it is probably the best thing to put a try/catch block around an action's implementation.
